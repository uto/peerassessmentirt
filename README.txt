The programs are the parameter estimation codes for the item response model which incorporates the rater characteristic parameters. 
The codes are written in Java.

To run the program, please use "/main/Main.java" file.
Before running the program, please confirm the configurations according to the comments in the Main.java file.

In this programs, you can estimate the parameters using your data set.
Sample data files are stored in "/data" directory.
Furthermore, random data can be generated using the "/main/Main.java" code. (See the comments in Main.java)

The data files should be named {rater1.csv, rater2.csv, ... ,rater[R].csv}.
Then, they should be stored in a directory as a csv format.
In each rater's file, each row indicate leaner; each column indicates assignment.
For example, the rating of row2-column1 in rater1.csv indicates "rater1's rating for assignment1 of learner2".
The ratings in the files takes a value in {0,1,2,3,4}. (In this programs, the number of rating categories is fixed to 5.)