package utility;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

public class InverseWishart {
	private int df;
	private double[][] S;
	
	public InverseWishart(int df, double[][] S){
		this.df = df;
		RealMatrix s4iw = new Array2DRowRealMatrix(S);
		RealMatrix pInverse = new LUDecomposition(s4iw).getSolver().getInverse();
		this.S = pInverse.getData();
	}
	
	public double[][] get_sample(){
		int d = S.length;
		double[] mean = new double[d];
		RealMatrix W_matrix = new Array2DRowRealMatrix(new double[d][d]);
		for (int i = 0; i < d; i++) { mean[i] = 0.0; }
		MultivariateNormalDistribution gaussian = new MultivariateNormalDistribution(mean, S);
		for (int i = 0; i < df; i++) {
			double[] r = gaussian.sample();
			RealMatrix r_rm = new Array2DRowRealMatrix(r);
			r_rm = r_rm.multiply(r_rm.transpose());
			W_matrix = W_matrix.add(r_rm);
		}
		RealMatrix p2Inverse = new LUDecomposition(W_matrix).getSolver().getInverse();
		double[][] IW = p2Inverse.getData();
		return IW;
	}
}
