package utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;

public class Util {

	public double getGausianValue(double m, double s, double v) {
		double ee = - Math.pow(v - m, 2) / (2 * s * s);
		return Math.exp(ee) / (Math.sqrt(2 * Math.PI) * s);
	}

	public double getMultGausianValue(double[] m, double[][] s, double[] v) {
		MultivariateNormalDistribution mnd = new MultivariateNormalDistribution(m, s);
		return mnd.density(v);
	}
	
	public double[] parseDimensionGRM(double[] v){
		double[] d = new double[v.length-1];
		for (int i=0; i< d.length; i++){
			d[i] = v[i];
		}
		return d;
	}

	public boolean isRejected(double thresh, Random rand) {
		boolean isRejected = false;
		if (thresh < 1.0) {
			double t = rand.nextDouble();
			if (t > thresh) {
				isRejected = true;
			}
		}
		return isRejected;
	}

	public void print(String str) {
		System.out.println(str);
	}

	public String form(double d, int length) {
		DecimalFormat df = new DecimalFormat();
		df.applyPattern("0");
		df.setMaximumFractionDigits(length);
		df.setMinimumFractionDigits(length);
		Double objnum = new Double(d);
		return df.format(objnum);
	}
	
	public PrintWriter Writer(String fileName) throws IOException {
		return new PrintWriter(new BufferedWriter(new FileWriter(new File(fileName))));
	}

	public BufferedReader Reader(String fileName) throws FileNotFoundException {
		return new BufferedReader(new FileReader(new File(fileName)));
	}

	public String doublesTostring(double[] t, int digit) {
		String line = "";
		for (int i = 0; i < t.length; i++) {
			line += form(t[i], digit) + ",";
		}
		return line.substring(0, line.length() - 1);
	}
	
	// used in MCMC process
	public double[] getAverages(ArrayList<double[]> array) {
		int l = array.get(0).length;
		double[] ave = new double[l];
		for (int i = 0; i < array.size(); i++) {
			for (int j = 0; j < l; j++) {
				ave[j] += array.get(i)[j];
			}
		}
		for (int j = 0; j < l; j++) {
			ave[j] /= array.size();
		}
		return ave;
	}
	
	public double[][] getAveragesMatrix(ArrayList<double[][]> array) {
		int l = array.get(0).length;
		int c = array.get(0)[0].length;
		double[][] ave = new double[l][c];
		for (int i = 0; i < array.size(); i++) {
			for (int j = 0; j < l; j++) {
				for (int k = 0; k < c; k++) {
					ave[j][k] += array.get(i)[j][k];
				}
			}
		}
		for (int j = 0; j < l; j++) {
			for (int k = 0; k < c; k++) {
				ave[j][k] /= array.size();
			}
		}
		return ave;
	}
		
	// used for calculating Standard Error of MCMC estimates
	public double[] getStandardError(ArrayList<double[]> array, double[] ave) {
		double[] SD = new double[ave.length];
		for (int i = 0; i < array.size(); i++) {
			for (int j = 0; j < SD.length; j++) {
				SD[j] += Math.pow(array.get(i)[j] - ave[j], 2);
			}
		}
		for (int j = 0; j < SD.length; j++) {
			SD[j] = Math.sqrt(SD[j]/array.size());
		}
		return SD;
	}

	public double[][] getStandardErrorMatrix(ArrayList<double[][]> array, double[][] aves) {
		int l = aves.length;
		int c = aves[0].length;
		double[][] SD = new double[l][c];
		for (int i = 0; i < array.size(); i++) {
			for (int j = 0; j < l; j++) {
				for (int k = 0; k < c; k++) {
					SD[j][k] += Math.pow(array.get(i)[j][k] - aves[j][k], 2);
				}
			}
		}
		for (int j = 0; j < l; j++) {
			for (int k = 0; k < c; k++) {
				SD[j][k] = Math.sqrt(SD[j][k] / array.size());
			}
		}
		return SD;
	}
}
