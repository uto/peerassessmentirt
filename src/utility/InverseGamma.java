package utility;

import java.util.Random;

public class InverseGamma {
	// InvertGamma = 1.0 / Gamma(shape, 1/scale)
	private double shape;
	private double scale;
	
	public InverseGamma(double shape, double scale){
		this.shape = shape;
		this.scale = 1.0 / scale;
	}

	public double get_sample() {
		double n, x, g, b1, b2, c1, c2, y, v1, v2, w1, w2;
		Random r = new Random();
		if (shape <= 0.4) n = 1.0 / shape;
		else if (shape <= 4) n = 1.0 / shape + (shape - 0.4) / (3.6 * shape);
		else n = 1.0 / Math.sqrt(shape);
		b1 = shape - 1 / n;
		b2 = shape + 1 / n;
		if (shape <= 0.4) c1 = 0;
		else c1 = b1 * (Math.log(b1) - 1) / 2;
		c2 = b2 * (Math.log(b2) - 1) / 2;
		y = 0.0; x = 0.0;
		w1 = w2 = 0.0;
		do {
			do {
				v1 = r.nextDouble();
				v2 = r.nextDouble();
				w1 = c1 + Math.log(v1);
				w2 = c2 + Math.log(v2);
				y = n * (b1 * w2 - b2 * w1);
			} while (y < 0);
			x = n * (w2 - w1);
		} while (Math.log(y) < x);
		g = scale * Math.exp(x);
		return 1.0 / g;
	}
}
