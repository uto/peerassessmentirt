package irt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.uncommons.maths.random.GaussianGenerator;

import utility.InverseGamma;
import utility.InverseWishart;
import utility.Util;

public class IRTModel {
	private Util util;
	private Random rand;
	private Data data;
	public int I, J, R, K;

	double[] alpha, alphaR, tauR, theta;
	double[][] beta_ik;

	// True Hyper Prior Distribution
	public final double[][] hyperprior_alpha = {{0, 0.1},{18, 5}};
	public final double[][] hyperprior_tau = {{0, 0.4},{8, 5}};
	public final double[] hyperprior_betaIK_mu_mean = {-2.0, -0.75, 0.75, 2.0};
	public final double[][] hyperprior_betaIK_mu_var = {
			{0.16, 0.10, 0.04, 0.04},
			{0.10, 0.16, 0.10, 0.04},
			{0.04, 0.10, 0.16, 0.10},
			{0.04, 0.04, 0.10, 0.16}};
	public final double[][] hyperprior_betaIK_S_scale_matrix = {
			{3, 2, 2, 2},
			{2, 3, 2, 2},
			{2, 2, 3, 2},
			{2, 2, 2, 3}};
	public final int hyperprior_betaIK_S_df = 10;
	
	// True Prior Distribution
	public double[] alpha_prior = { 0.1, 0.4 };
	public double[] alphaR_prior = { 0.0, 0.5 };
	public double[] tauR_prior = { 0.0, 0.8 };
	public double[] betaIK_prior_mu = hyperprior_betaIK_mu_mean.clone();
	public double[][] betaIK_prior_sigma = hyperprior_betaIK_mu_var.clone();
	final double[] theta_prior = { 0.0, 1.0 };

	public IRTModel(int item, int examinee, int rater, int categories, Random rand) {
		this.rand = rand;
		this.util = new Util();
		this.I = item;
		this.J = examinee;	
		this.R = rater;
		this.K = categories;
		init_parameters();
	}
	
	void init_parameters(){
		alpha = new double[I];
		alphaR = new double[R];
		beta_ik  = new double[I][K];
		tauR  = new double[R];
		theta = new double[J];
		
		alphaR[0] = alphaR_prior[0];
		tauR[0] = tauR_prior[0];
	}

	public void setRandomParameters() {
		setTheta();
		setItemParameters();
		setRaterParameters();
	}

	public void setTheta() {
		GaussianGenerator gen = new GaussianGenerator(theta_prior[0], theta_prior[1], rand);
		for (int i = 0; i < J; i++) {
			theta[i] = gen.nextValue();
		}
	}

	public void setRaterParameters() {
		GaussianGenerator AlphaR_gen = new GaussianGenerator(alphaR_prior[0], alphaR_prior[1], rand);		
		GaussianGenerator TauR_gen = new GaussianGenerator(tauR_prior[0], tauR_prior[1], rand);
		for (int r = 1; r < R; r++) {
			alphaR[r] = AlphaR_gen.nextValue();
			tauR[r] = TauR_gen.nextValue();
		}
	}

	public void setItemParameters() {
		GaussianGenerator AlphaGenerator = new GaussianGenerator(alpha_prior[0], alpha_prior[1], rand);
		MultivariateNormalDistribution gaussian = new MultivariateNormalDistribution(betaIK_prior_mu, betaIK_prior_sigma);
		for (int i = 0; i < I; i++) {
			alpha[i] = AlphaGenerator.nextValue();
			double[] betaCandidate = gaussian.sample();
			Arrays.sort(betaCandidate);
			for (int k = 0; k < betaCandidate.length; k++) {
				beta_ik[i][k] = betaCandidate[k];
			}
			beta_ik[i][beta_ik[i].length - 1] = 0.0;
		}
	}

	double[] get_random_hyperprior_normal(double[][] hyperprior){
		double[] newPrior = new double[2];
		GaussianGenerator gen = new GaussianGenerator(hyperprior[0][0], hyperprior[0][1], rand);
		newPrior[0] = gen.nextValue();
		InverseGamma ig = new InverseGamma(hyperprior[1][0], hyperprior[1][1]);
		newPrior[1] = Math.sqrt(ig.get_sample());
		return newPrior;
	}
	
	public void set_random_hyperparam(){
		alpha_prior = get_random_hyperprior_normal(hyperprior_alpha);
		alphaR_prior = get_random_hyperprior_normal(hyperprior_alpha);
		tauR_prior = get_random_hyperprior_normal(hyperprior_tau);
		MultivariateNormalDistribution gaussian = new MultivariateNormalDistribution(hyperprior_betaIK_mu_mean, hyperprior_betaIK_mu_var);
		while(true){
			boolean violated = false;
			double[] mSample = gaussian.sample();
			for(int k=0;k<hyperprior_betaIK_mu_mean.length-1;k++){
				if(mSample[k] > mSample[k+1] || Math.abs(mSample[k] - mSample[k+1]) < 0.2){
					violated = true;
					break;
				}
			}
			if(!violated){
				betaIK_prior_mu = mSample.clone();
				break;
			}
		}
		while(true){
			InverseWishart iw = new InverseWishart(hyperprior_betaIK_S_df, hyperprior_betaIK_S_scale_matrix);
			double[][] sSample = iw.get_sample();
			boolean violated = false;
			for(int k=0; k<K-1; k++){
				betaIK_prior_sigma[k] = sSample[k].clone();
				if(sSample[k][k] < 0.04 || sSample[k][k] > 1.0){
					violated = true;
					break;
				}
			}
			if(!violated){ break;}
		}
	}

	double[] IRC(int i, double theta, int r) {
		double[] BCC = new double[K - 1];
		double[] IRC = new double[K];
		for (int k = 0; k < K; k++) {
			if (k == 0) {
				BCC[k] = BCC(i, theta, r, k);
				IRC[k] = 1.0 - BCC[k];
			} else if (k == (K - 1)) {
				IRC[k] = BCC[k - 1];
			} else {
				BCC[k] = BCC(i, theta, r, k);
				IRC[k] += BCC[k - 1] - BCC[k];
			}
		}
		return IRC;
	}
	
	double getIrcK(int i, double theta, int r, int k){
		if(k == 0){
			return 1.0 - BCC(i, theta, r, k);
		}else if(k == (K-1)){
			return BCC(i, theta, r, k-1);
		}else{
			return BCC(i, theta, r, k-1) - BCC(i, theta, r, k);
		}
	}
	

	double BCC(int i, double theta, int r, int k){
		double logit = - (Math.exp(alpha[i]) * Math.exp(alphaR[r])) * (theta - beta_ik[i][k] - tauR[r]);
		return 1.0 / (1.0 + Math.exp(logit));
	}

	double getProb(int j, int i, int r) {
		int k = this.data.U[j][i][r];
		if (k != -1) {
			return getIrcK(i, theta[j], r, k);
		}
		return 1.0;			
	}

	double getLogLikelihood() {
		double LogLikelihood = 0.0;
		for (int j = 0; j < J; j++) {
			LogLikelihood += LogLikelihoodTheta(j);
		}
		return LogLikelihood;
	}

	double LogLikelihoodTheta(int j) {
		double LogLikelihood = 0.0;
		for (int i = 0; i < I; i++) {
			for (int r = 0; r < R; r++) {
				LogLikelihood += Math.log(getProb(j, i, r));
			}
		}
		return LogLikelihood;
	}

	double LogLikelihoodItem(int i) {
		double LogLikelihood = 0.0;
		for (int j = 0; j < J; j++) {
			for (int r = 0; r < R; r++) {
				LogLikelihood += Math.log(getProb(j, i, r));
			}
		}
		return LogLikelihood;
	}

	double LogLikelihoodRater(int r) {
		double LogLikelihood = 0.0;
		for (int j = 0; j < J; j++) {
			for (int i = 0; i < I; i++) {
				LogLikelihood += Math.log(getProb(j, i, r));
			}
		}
		return LogLikelihood;
	}
	

	public double posterior_theta(int j, double[] prior){
		return LogLikelihoodTheta(j)+ Math.log(util.getGausianValue(prior[0], prior[1], theta[j]));
	}

	public double posterior_alpha_i(int i, double[] prior){
		return LogLikelihoodItem(i)+ Math.log(util.getGausianValue(prior[0], prior[1], alpha[i]));
	}

	public double posterior_beta_ik(int i, double[] prior_mu, double[][] prior_sigma){		
		return LogLikelihoodItem(i) + Math.log(util.getMultGausianValue(prior_mu, prior_sigma, util.parseDimensionGRM(beta_ik[i])));
	}

	public double posterior_beta_ik(int i, int k, double[] prior_mu, double[][] prior_sigma){	
		double LogLikelihood = 0.0;
		for(int j=0;j<J;j++){
			for(int r=0;r<R;r++){
				if(this.data.U[j][i][r] == k){
					LogLikelihood += Math.log(getIrcK(i, theta[j], r, k));
				}
			}
		}
		return LogLikelihood + Math.log(util.getMultGausianValue(prior_mu, prior_sigma, util.parseDimensionGRM(beta_ik[i])));
	}

	public double posterior_alpha_r(int r, double[] prior){
		return LogLikelihoodRater(r) + Math.log(util.getGausianValue(prior[0], prior[1], alphaR[r]));		
	}

	public double posterior_tau_r(int r, double[] prior){
		return LogLikelihoodRater(r) + Math.log(util.getGausianValue(prior[0], prior[1], tauR[r]));		
	}

	// data binding
	public void bindData(String inputFile) {
		this.data = new Data(this, inputFile, rand);
	}

	public Data getData(){
		return this.data;
	}

	public void setRatingData(int[][][] ratingData){
		this.data = new Data(this, null, rand);
		this.data.setRatingData(ratingData);
	}

	public int[][][] getRatingData(){
		Data curData = this.data;
		return curData.getRatingData();
	}

	void printParameters() {
		int digit = 3;
		util.print("  log alpha_i：" + util.doublesTostring(alpha, digit));
		util.print("  log alpha_r：" + util.doublesTostring(alphaR, digit));
		for (int i = 0; i < beta_ik.length; i++) {
			util.print("  betaIK(i="+i+")："+util.doublesTostring(beta_ik[i], digit));
		}
		util.print("  epsilon_r：" + util.doublesTostring(tauR, digit));
		util.print("  theta_j：" + util.doublesTostring(theta, digit));
		util.print("");
	}
	
	void printHyperParameters(){
		int digit = 3;
		util.print("Hyper Parameters");
		util.print("  alpha_prior：" + util.doublesTostring(alpha_prior, digit));
		util.print("  alphaR_prior：" + util.doublesTostring(alphaR_prior, digit));
		util.print("  betaIK_prior_mu：" + util.doublesTostring(betaIK_prior_mu, digit));
		for (int k = 0; k < betaIK_prior_sigma.length; k++) {
			util.print("  betaIK_prior_sigma(k=" + k + ")："
					+ util.doublesTostring(betaIK_prior_sigma[k], digit));
		}
		util.print("  epsilon_prior：" + util.doublesTostring(tauR_prior, digit));
		util.print("  theta_prior：" + util.doublesTostring(theta_prior, digit));		
	}
	
	public void outputParameters(String outputFilePath) {
		try {
			Util util = new Util();
			int digit = 3;
			PrintWriter bw;
			bw = util.Writer(outputFilePath + "/parameters.txt");
			bw.println("Parameters");
			bw.println("  log alpha_i：" + util.doublesTostring(alpha, digit));
			bw.println("  log alpha_r：" + util.doublesTostring(alphaR, digit));
			for (int i = 0; i < beta_ik.length; i++) {
				bw.println("  beta_ik(i=" + i + ")："
						+ util.doublesTostring(beta_ik[i], digit));
			}
			bw.println("  epsilon：" + util.doublesTostring(tauR, digit));
			bw.println("  theta_j：" + util.doublesTostring(theta, digit));
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
