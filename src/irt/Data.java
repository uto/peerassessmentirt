package irt;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.StringTokenizer;

import utility.Util;

public class Data {
	int[][][] U;
	private IRTModel irt;
	private Random rand;

	public Data(IRTModel irt, String inputFilePath, Random rand) {
		this.irt = irt;
		this.rand = rand;
		U = new int[irt.J][irt.I][irt.R];
		if (inputFilePath != null) {
			setRealData(inputFilePath);
		} else {
			createData();
		}
	}

	void createData() {
		for (int j = 0; j < U.length; j++) {
			for (int i = 0; i < U[j].length; i++) {
				for (int r = 0; r < U[j][i].length; r++) {
					double[] cumulativeIRC = getCumulativeProb(irt.IRC(i, irt.theta[j], r));
					U[j][i][r] = randomChoice(cumulativeIRC);
				}
			}
		}
	}

	double[] getCumulativeProb(double[] IRC) {
		double[] cumulativeIRC = new double[IRC.length];
		cumulativeIRC[0] = IRC[0];
		for (int i = 1; i < IRC.length; i++) {
			cumulativeIRC[i] = cumulativeIRC[i - 1] + IRC[i];
		}
		return cumulativeIRC;
	}

	int randomChoice(double[] irc) {
		int cluster = 0;
		double t = rand.nextDouble();
		for (int k = 0; k < irc.length; k++) {
			if (t < irc[k]) {
				cluster = k;
				break;
			}
		}
		return cluster;
	}
	
	void setRatingData(int[][][] U_old){
		for(int j=0; j<U_old.length; j++){
			for(int i=0; i<U_old[j].length; i++){
				for(int r=0; r<U_old[j][i].length; r++){
					U[j][i][r] = U_old[j][i][r];
				}	
			}
		}
	}

	int[][][] getRatingData(){
		int[][][] Rating = new int[irt.J][irt.I][irt.R];
		for(int j=0; j<Rating.length; j++){
			for(int i=0; i<Rating[j].length; i++){
				for(int r=0; r<Rating[j][i].length; r++){
					Rating[j][i][r] = U[j][i][r];
				}	
			}
		}
		return Rating;
	}

	public void setRealData(String inputFilePath) {
		Util util = new Util();
		try {
			for (int r = 0; r < U[0][0].length; r++) {
				BufferedReader br;
				br = util.Reader(inputFilePath + "/rater" + (r+1) + ".csv");
				for (int j = 0; j < U.length; j++) {
					String str = br.readLine();
					if (str != null) {
						StringTokenizer st = new StringTokenizer(str, ",");
						for (int i = 0; i < U[j].length; i++) {
							U[j][i][r] = Integer.valueOf(st.nextToken());
						}
					}
				}
				br.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void outputData(String outputFilePath) {
		try {
			Util util = new Util();
			for (int r = 0; r < irt.R; r++) {
				PrintWriter bw = util.Writer(outputFilePath + "/rater" + (r+1) + ".csv");
				for (int j = 0; j < irt.J; j++) {
					String line = "";
					for (int i = 0; i < irt.I; i++) {
						line += U[j][i][r];
						if (i != irt.I - 1)
							line += ",";
					}
					bw.println(line);
				}
				bw.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
