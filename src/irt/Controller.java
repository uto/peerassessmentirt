package irt;

import java.util.HashMap;
import java.util.Random;

import org.uncommons.maths.random.MersenneTwisterRNG;

public class Controller {
	private int I, J, R, K;
	private HashMap<String, Integer> mcmcSettings;
	private Random rand;

	public Controller(int items, int learners, int raters, int categories,
			HashMap<String, Integer> mcmcSettings) {
		this.I = items;
		this.J = learners;
		this.R = raters;
		this.K = categories;
		this.mcmcSettings = mcmcSettings;
		rand = new MersenneTwisterRNG();
	}

	// generate simulation data and run parameter estimation using the data
	public void runSimulation() {
		IRTModel trueModel = new IRTModel(I, J, R, K, rand);
		trueModel.setRandomParameters();
		trueModel.bindData(null);

		IRTModel estModel = new IRTModel(I, J, R, K, rand);
		estModel.setRandomParameters();
		estModel.setRatingData(trueModel.getRatingData());
		Estimation est = new Estimation(estModel, rand, mcmcSettings);

		System.out.println("\r\n>>  Standard Errors");
		est.printStandardError();
		System.out.println("\r\n>> True Paramters");
		trueModel.printParameters();
		System.out.println(">> Estimated Paramters");
		estModel.printParameters();
	}

	// run parameter estimation using given data set
	public void runWithGivenData(String inputFilePath) {
		IRTModel estModel = new IRTModel(I, J, R, K, rand);
		estModel.setRandomParameters();
		estModel.bindData(inputFilePath);
		System.out.println("Binded data files stored in [/"+inputFilePath+"] directory.");
		Estimation est = new Estimation(estModel, rand, mcmcSettings);

		System.out.println("\r\n>>  Standard Errors");
		est.printStandardError();
		System.out.println("\r\n>> Estimated Paramters");
		estModel.printParameters();
	}

	// generate simulation data and output the data set
	public void generateSimulationData(String outputFilePath) {
		IRTModel trueModel = new IRTModel(I, J, R, K, rand);
		trueModel.setRandomParameters();
		trueModel.bindData(null);

		trueModel.outputParameters(outputFilePath);
		trueModel.getData().outputData(outputFilePath);
		System.out.println("Generated data files were saved to [/"+outputFilePath+"] directory.\r\n");
	}
}
