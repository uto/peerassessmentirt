package irt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.uncommons.maths.random.GaussianGenerator;

import utility.InverseGamma;
import utility.InverseWishart;
import utility.Util;

public class Estimation {
	Util util;

	ArrayList<double[]> Alpha_Ests, AlphaR_Ests, TauR_Ests, Theta_Ests;
	ArrayList<double[][]> BetaIK_Ests;

	// MCMC samples for Hyper Parameters
	ArrayList<double[]> HP_Alpha_Ests, HP_AlphaR_Ests, HP_TauR_Ests, HP_BetaMu_Ests;
	ArrayList<double[][]> HP_BetaSigma_Ests;
	
	// Standard Error of Parameters
	public double[] SE_Alpha_Ests, SE_AlphaR_Ests, SE_TauR_Ests, SE_Theta_Ests;
	public double[][] SE_Beta_Ests;	
		
	private int MaxMCMC, burnIn, interbal;
	IRTModel irt;
	Random rand;
	final double prop_sd = 0.1;

	Estimation(IRTModel irt, Random rand, HashMap<String, Integer> settings) {
		this.irt = irt;
		this.rand = rand;
		this.MaxMCMC = settings.get("MaxMCMC");
		this.burnIn = settings.get("burnIn");
		this.interbal = settings.get("interbal");
		this.util = new Util();

		Alpha_Ests = new ArrayList<double[]>();
		AlphaR_Ests = new ArrayList<double[]>();
		BetaIK_Ests = new ArrayList<double[][]>();
		TauR_Ests = new ArrayList<double[]>();
		Theta_Ests = new ArrayList<double[]>();

		// Hyper Parameters
		HP_Alpha_Ests = new ArrayList<double[]>();
		HP_AlphaR_Ests = new ArrayList<double[]>();
		HP_BetaMu_Ests = new ArrayList<double[]>();
		HP_BetaSigma_Ests = new ArrayList<double[][]>();
		HP_TauR_Ests = new ArrayList<double[]>();
		
		util.print("Start Parameter Estimation.");
		for (int i = 0; i < this.MaxMCMC; i++) {
			run_update();
			run_update_hp(irt, util, rand);
			if (i >= this.burnIn && (i % this.interbal) == 0) {
				this.addMCMCSamples();
				this.addMCMC_hyperparameters();					
			}
			if (i % 500 == 0) util.print("MCMC Roop: " + i + " / " + MaxMCMC);				
		}
		calcPointEstimation();
		calcPointEstimationHyperparameters();
	}

	void addMCMCSamples() {
		Alpha_Ests.add(irt.alpha.clone());
		AlphaR_Ests.add(irt.alphaR.clone());
		double[][] betaIK_tmp = new double[irt.beta_ik.length][];
		for (int i = 0; i < irt.beta_ik.length; i++) {
			betaIK_tmp[i] = irt.beta_ik[i].clone();
		}
		BetaIK_Ests.add(betaIK_tmp);
		TauR_Ests.add(irt.tauR.clone());
		Theta_Ests.add(irt.theta.clone());
	}

	void addMCMC_hyperparameters(){
		HP_Alpha_Ests.add(irt.alpha_prior.clone());
		HP_AlphaR_Ests.add(irt.alphaR_prior.clone());
		HP_TauR_Ests.add(irt.tauR_prior.clone());
		HP_BetaMu_Ests.add(irt.betaIK_prior_mu.clone());
		double[][] hp_beta_tmp = new double[irt.K-1][];
		for(int k=0;k<irt.K-1;k++){
			hp_beta_tmp[k] = irt.betaIK_prior_sigma[k].clone();
		}
		HP_BetaSigma_Ests.add(hp_beta_tmp);		
	}
	
	void run_update(){
		updateThetaParam();
		updateAlphaI();
		updateBetaIK();
		updateRaterSeverity();
		updateRaterConsistency();
	}
	
	public void run_update_hp(IRTModel irt, Util util, Random rand){
		irt.alpha_prior = updateNG(irt.alpha, irt.alpha_prior, irt.hyperprior_alpha, rand);
		this.updateNMG(irt);
		irt.alphaR_prior = updateNG(irt.alphaR, irt.alphaR_prior, irt.hyperprior_alpha, rand);
		irt.tauR_prior = updateNG(irt.tauR, irt.tauR_prior, irt.hyperprior_tau, rand);
	}
	
	void calcPointEstimation() {
		irt.alpha = util.getAverages(Alpha_Ests);
		irt.alphaR = util.getAverages(AlphaR_Ests);
		irt.beta_ik = util.getAveragesMatrix(BetaIK_Ests);
		irt.tauR = util.getAverages(TauR_Ests);
		irt.theta = util.getAverages(Theta_Ests);
		// set Standard Errors
		SE_Alpha_Ests = util.getStandardError(Alpha_Ests, irt.alpha);
		SE_Beta_Ests = util.getStandardErrorMatrix(BetaIK_Ests, irt.beta_ik);
		SE_AlphaR_Ests = util.getStandardError(AlphaR_Ests, irt.alphaR);
		SE_TauR_Ests = util.getStandardError(TauR_Ests, irt.tauR);
		SE_Theta_Ests = util.getStandardError(Theta_Ests, irt.theta);
	}
	
	void calcPointEstimationHyperparameters(){
		irt.alpha_prior = util.getAverages(HP_Alpha_Ests);
		irt.betaIK_prior_mu = util.getAverages(HP_BetaMu_Ests);	
		irt.betaIK_prior_sigma = util.getAveragesMatrix(HP_BetaSigma_Ests);
		irt.alphaR_prior = util.getAverages(HP_AlphaR_Ests);
		irt.tauR_prior = util.getAverages(HP_TauR_Ests);
	}

	public void updateAlphaI() {
		for(int i=0;i<irt.I;i++){
			double prevAlpha = irt.alpha[i];
		    try{
				double prevLikelihood = irt.posterior_alpha_i(i, irt.alpha_prior);
				GaussianGenerator gg = new GaussianGenerator(prevAlpha, prop_sd, rand);
				irt.alpha[i] = gg.nextValue();
				double newLikelihood = irt.posterior_alpha_i(i, irt.alpha_prior);
				double thresh = Math.exp(newLikelihood - prevLikelihood);
				if(thresh > 1.0) thresh = 1.0;
				if(util.isRejected(thresh, rand) == true){
					irt.alpha[i] = prevAlpha;
				}
		    } catch (Exception e) {
				irt.alpha[i] = prevAlpha;
		    }
		}
	}

	public void updateBetaIK() {
		for(int i=0;i<irt.I;i++){
			double prevBeta[] = irt.beta_ik[i].clone();
		    try{
				double prevLikelihood = irt.posterior_beta_ik(i, irt.betaIK_prior_mu, irt.betaIK_prior_sigma);
				for(int k=0;k<irt.K-1;k++){
					GaussianGenerator gg = new GaussianGenerator(irt.beta_ik[i][k], prop_sd, rand);
					irt.beta_ik[i][k] = gg.nextValue();
					if(k==0){
						if(irt.beta_ik[i][k] > irt.beta_ik[i][k+1]){
							irt.beta_ik[i][k] = prevBeta[k];
						}				
					}else if(k == (irt.K-2)){
						if(irt.beta_ik[i][k-1] > irt.beta_ik[i][k]){
							irt.beta_ik[i][k] = prevBeta[k];
						}
					}else{
						if(irt.beta_ik[i][k-1] > irt.beta_ik[i][k] || irt.beta_ik[i][k] > irt.beta_ik[i][k+1]){
							irt.beta_ik[i][k] = prevBeta[k];
						}
					}
				}
				double newLikelihood = irt.posterior_beta_ik(i, irt.betaIK_prior_mu, irt.betaIK_prior_sigma);
				double thresh = Math.exp(newLikelihood - prevLikelihood);
				if(thresh > 1.0) thresh = 1.0;
				if( util.isRejected(thresh, rand) == true){
					irt.beta_ik[i] = prevBeta.clone();
				}			
		    } catch (Exception e) {
				irt.beta_ik[i] = prevBeta.clone();
		    }
		}
	}

	public void updateRaterConsistency() {
		for(int r=1; r<irt.R; r++){
			double prevAlphaR = irt.alphaR[r];
		    try{
				double prevLikelihood = irt.posterior_alpha_r(r, irt.alphaR_prior);
				GaussianGenerator gg = new GaussianGenerator(irt.alphaR[r], prop_sd, rand);
				irt.alphaR[r] = gg.nextValue();
				double newLikelihood = irt.posterior_alpha_r(r, irt.alphaR_prior);
				double thresh = Math.exp(newLikelihood - prevLikelihood);
				if(thresh > 1.0) thresh = 1.0;
				if( util.isRejected(thresh, rand) == true){
					irt.alphaR[r] = prevAlphaR;
				}
		    } catch (Exception e) {
				irt.alphaR[r] = prevAlphaR;
		    }
		}	}

	public void updateRaterSeverity() {
		for(int r=1; r<irt.R; r++){
			double prevTauR = irt.tauR[r];
		    try{
				double prevLikelihood = irt.posterior_tau_r(r, irt.tauR_prior);
				GaussianGenerator gg = new GaussianGenerator(irt.tauR[r], prop_sd, rand);
				irt.tauR[r] = gg.nextValue();
				double newLikelihood = irt.posterior_tau_r(r, irt.tauR_prior);
				double thresh = Math.exp(newLikelihood - prevLikelihood);
				if(thresh > 1.0) thresh = 1.0;
				if( util.isRejected(thresh, rand) == true){
					irt.tauR[r] = prevTauR;
				}
		    } catch (Exception e) {
				irt.tauR[r] = prevTauR;
		    }
	    }
	}

	public void updateThetaParam() {
		for (int j = 0; j < irt.J; j++) {
			double prevTheta = irt.theta[j];
		    try{
				double prevLikelihood = irt.posterior_theta(j, irt.theta_prior);
				GaussianGenerator gg = new GaussianGenerator(prevTheta, prop_sd, rand);
				irt.theta[j] = gg.nextValue();
				double newLikelihood = irt.posterior_theta(j, irt.theta_prior);
				double thresh = Math.exp(newLikelihood - prevLikelihood);
				if(thresh > 1.0) thresh = 1.0;
				if (util.isRejected(thresh, rand) == true) {
					irt.theta[j] = prevTheta;
				}
		    } catch (Exception e) {
				irt.theta[j] = prevTheta;
		    }
		}
	}

	public double[] updateNG(double[] params, double[] prior, double[][] hyperprior, Random rand){
		double[] newprior = new double[2];

		// update mean of the normal prior
		double n = (double)params.length;
		double ave = 0.0;
		for(int j=0;j<n;j++){ ave += params[j]; }
		ave /= n;
		double n0 = 1.0;
		double nk = n0 + n;
		double m = (n0 * hyperprior[0][0] + n * ave) / nk;
		double s = Math.pow(prior[1], 2) / nk;
		GaussianGenerator ggh = new GaussianGenerator(m, s, rand);
		newprior[0] = ggh.nextValue();

		// update SD of the normal prior
		double shape = hyperprior[1][0] +  n / 2.0;
		double s2 = 0.0;
		for(int j=0; j<n; j++){ s2 += Math.pow(params[j] - newprior[0], 2);}
		double scale = hyperprior[1][1] + s2 / 2 + n0 * n * Math.pow(ave - hyperprior[0][0], 2) / (2 * nk);
		InverseGamma ig = new InverseGamma(shape, scale);
		newprior[1] = Math.sqrt(ig.get_sample());
		return newprior;
	}

	public void updateNMG(IRTModel irt){
		// sample mean of mult gaussian
		int I0 = 1;
		double[] ave = new double[irt.K-1];
		for(int k=0; k<irt.K-1; k++){
			for(int i=0; i<irt.I; i++){
				ave[k] += irt.beta_ik[i][k]/(double)irt.I;
			}
		}
		double[] m = new double[irt.K-1];
		for(int k=0; k<irt.K-1; k++){
			m[k] = (I0 * irt.hyperprior_betaIK_mu_mean[k] + irt.I * ave[k]) / (irt.I + I0);
		}
		double[][] s = new double[irt.K-1][irt.K-1];
		for(int k=0; k<irt.K-1; k++){
			for(int kk=0; kk<irt.K-1; kk++){
				s[k][kk] = irt.betaIK_prior_sigma[k][kk]/(irt.I + I0);
			}
		}
		MultivariateNormalDistribution gaussian = new MultivariateNormalDistribution(m, s);
		double[] newprior_mu = gaussian.sample();

		// sample sigma of mult gaussian
		int nu = irt.hyperprior_betaIK_S_df + irt.I; 
		double[][] SS_star = new double[irt.K-1][irt.K-1];
		for(int k=0; k<irt.K-1; k++){
			for(int kk=k; kk<irt.K-1; kk++){
				double SS = 0;
				for(int i=0; i<irt.I; i++){
					SS += (irt.beta_ik[i][k] - ave[k]) * (irt.beta_ik[i][kk] - ave[kk]);
				}
				double SS_3rd_eq = (ave[k] - irt.hyperprior_betaIK_mu_mean[k]) * (ave[kk] - irt.hyperprior_betaIK_mu_mean[kk]);
				SS_3rd_eq *= irt.I * I0 / (irt.I + I0);

				SS_star[k][kk] = irt.hyperprior_betaIK_S_scale_matrix[k][kk] + SS + SS_3rd_eq;
				if(k != kk) SS_star[kk][k] = SS_star[k][kk];
			}
		}
		InverseWishart iw = new InverseWishart(nu, SS_star);
		double[][] newprior_sigma = iw.get_sample();

		// Check Constraint
		boolean violated = false;
		for(int k=0;k<irt.K-2;k++){
			if(newprior_mu[k] > newprior_mu[k+1]){
				violated = true; break;
			}
		}
		if(!violated){
			irt.betaIK_prior_mu = newprior_mu;
			irt.betaIK_prior_sigma = newprior_sigma;
		}
	}
	
	void printStandardError() {
		int digit = 3;
		util.print(">> Standard Error ");
		util.print("  log alpha_i：" + util.doublesTostring(SE_Alpha_Ests, digit));
		util.print("  log alpha_r：" + util.doublesTostring(SE_AlphaR_Ests, digit));
		for (int i = 0; i < SE_Beta_Ests.length; i++) {
			util.print("  betaIK(i="+i+")："+util.doublesTostring(SE_Beta_Ests[i], digit));
		}
		util.print("  epsilon_r：" + util.doublesTostring(SE_TauR_Ests, digit));
		util.print("  theta_j：" + util.doublesTostring(SE_Theta_Ests, digit));
		util.print("");
	}
}
