package main;

import irt.Controller;

import java.io.IOException;
import java.util.HashMap;

public class Main {

	public static void main(String[] args) throws IOException {

		final int items = 5; // the number of assignment: items > 2
		final int learners = 20; // the number of learners: learners > 2
		final int raters = 20; // the number of raters: raters > 2: "raters = learner" in peer assessment
		final int categories = 5; // The number of rating categories: (must be fixed to 5)

		HashMap<String, Integer> mcmcSettings = new HashMap<String, Integer>();
		mcmcSettings.put("MaxMCMC", 50000); // Maximum chain length of MCMC (MaxMCMC > 2000)
		mcmcSettings.put("burnIn", 30000); // Burn-in length of MCMC: burnIn < MaxMCMC
		mcmcSettings.put("interbal", 500); // Interval length of MCMC

		Controller control = new Controller(items, learners, raters, categories, mcmcSettings);

		/* ---------------------------------------------------------
		  1. Use bellow codes to generate random data files from this IRT
		     model :the files will saved under "outputFilePath" directory.
		--------------------------------------------------------- */
		// final String outputFilePath = "data";
		// control.generateSimulationData(outputFilePath);

	
		/* ---------------------------------------------------------
		  2. Use bellow codes to estimate the model parameters using rating
		     data files saved in "inputDataPath" directory.
		---------------------------------------------------------*/
		// final String inputDataPath = "data";
		// control.runWithGivenData(inputDataPath);


		/* ---------------------------------------------------------		
		  3. Use bellow code to generate random data and run parameter
		     estimation using the generated data.
		---------------------------------------------------------*/
		control.runSimulation();
	}
}
